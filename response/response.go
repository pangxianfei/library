package response

func Json(code int, message string, data interface{}, success bool) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: success,
	}
}

func JsonData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}
//创建成功返回专用
func JsonCreateSucces(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "创建成功",
		Data:    data,
		Success: true,
	}
}
//创建失败返回专用
func JsonCreateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "创建失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

//查询返回专用
func JsonQueryData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "查询成功",
		Data:    data,
		Success: true,
	}
}
//更新成功返回专用
func JsonUpdateSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新成功",
		Data:    data,
		Success: true,
	}
}

func JsonUpdateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}



func JsonDeleteSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "删除成功",
		Data:    data,
		Success: true,
	}
}

func JsonDeleteFail(data string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "删除失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}


func JsonItemList(data []interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}

func JsonSuccess() *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "请求成功",
		Data:    nil,
		Success: true,
	}
}

func JsonErrorMsg(message string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonError(err error) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: err.Error(),
		Data:    nil,
		Success: false,
	}
}

//请求失败专用
func JsonFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "请求失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

func JsonErrorCode(code int, message string) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonErrorData(code int, message string, data interface{}) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: false,
	}
}

func JsonDataError(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    401,
		Message: "提交失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}


type RspBuilder struct {
	Data map[string]interface{}
}

func (builder *RspBuilder) Put(key string, value interface{}) *RspBuilder {
	builder.Data[key] = value
	return builder
}

func (builder *RspBuilder) Build() map[string]interface{} {
	return builder.Data
}

func (builder *RspBuilder) JsonResult() *JsonResult {
	return JsonData(builder.Data)
}
