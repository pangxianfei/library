package config

import (
	"io/ioutil"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var Instance *Config

type Config struct {
	AppName    string `yaml:"AppName"`    // 应用名
	Env        string `yaml:"Env"`        // 环境：prod、dev
	BaseUrl    string `yaml:"BaseUrl"`    // base url
	Port       string `yaml:"Port"`       // 端口
	LogFile    string `yaml:"LogFile"`    // 日志文件
	ShowSql    bool   `yaml:"ShowSql"`    // 是否显示日志
	StaticPath string `yaml:"StaticPath"` // 静态文件目录
	TenantLog  bool   `yaml:"TenantLog"`  // 是否显示控制台日志
	TenantDb   bool   `yaml:"TenantDb"`   // 是否显示控制台日志
	AppLocale  string `yaml:"AppLocale"`  //默认语言
	// 应用
	App struct {
		SysApp      string `yaml:"SysApp"`      //平台管理中心
		UserApp     string `yaml:"UserApp"`     //人事管理系统
		ProductApp  string `yaml:"ProductApp"`  //产品BOM
		OrderApp    string `yaml:"OrderApp"`    //订单系统
		PdApp       string `yaml:"PdApp"`       //生产
		WmApp       string `yaml:"WmApp"`       //仓库
		SupplierApp string `yaml:"SupplierApp"` //供应商
		NbomApp     string `yaml:"NbomApp"`     //非BOM
		LoginApp    string `yaml:"LoginApp"`    //登陆
		SrmApp      string `yaml:"SrmApp"`      //采购管理系统
		FmsApp      string `yaml:"FmsApp"`      //财务
		OaApp       string `yaml:"OaApp"`       //OA办公系统
	} `yaml:"App"`

	// 应用端口
	AppPort struct {
		SysPort      string `yaml:"SysAppPort"`   //平台管理中心
		UserPort     string `yaml:"UserPort"`     //人事管理系统
		ProductPort  string `yaml:"ProductPort"`  //产品BOM
		OrderPort    string `yaml:"OrderPort"`    //订单系统
		PdPort       string `yaml:"PdPort"`       //生产
		WmPort       string `yaml:"WmPort"`       //仓库
		SupplierPort string `yaml:"SupplierPort"` //供应商
		NbomPort     string `yaml:"NbomPort"`     //非BOM
		SrmPort      string `yaml:"SrmPort"`      //采购管理系统
		FmsPort      string `yaml:"FmsPort"`      //财务
		LoginPort    string `yaml:"LoginPort"`    //登陆
		OaPort       string `yaml:"OaPort"`       //Oa管理系统
	} `yaml:"AppPort"`

	// 应用编号
	AppNo struct {
		Sys      int64 `yaml:"Sys"`      //平台管理中心
		User     int64 `yaml:"User"`     //人事管理系统
		Product  int64 `yaml:"Product"`  //产品BOM
		Order    int64 `yaml:"Order"`    //订单系统
		Pd       int64 `yaml:"Pd"`       //生产
		Wm       int64 `yaml:"Wm"`       //仓库
		Supplier int64 `yaml:"Supplier"` //供应商
		Fms      int64 `yaml:"Fms"`      //财务
		Nbom     int64 `yaml:"Nbom"`     //非BOM
		Srm      int64 `yaml:"Srm"`      //采购管理系统
		Login    int64 `yaml:"Login"`    //登陆
		Oa       int64 `yaml:"Oa"`       //Oa管理系统
	} `yaml:"AppNo"`

	// 数据库配置
	DB struct {
		Dns          string `yaml:"Dns"`
		Host         string `yaml:"Host"`
		DbName       string `yaml:"DbName"`
		UserName     string `yaml:"UserName"`
		Password     string `yaml:"Password"`
		Prefix       string `yaml:"Prefix"`
		DbPort       int    `yaml:"DbPort"`
		Charset      string `yaml:"Charset"`
		MaxIdleConns int    `yaml:"MaxIdleConns"`
		MaxOpenConns int    `yaml:"MaxOpenConns"`
	} `yaml:"DB"`

	// 数据库配置
	MSSQLDB struct {
		Sqlserver          string `yaml:"Sqlserver"`
		Host               string `yaml:"Host"`
		DbName             string `yaml:"DbName"`
		UserName           string `yaml:"UserName"`
		Password           string `yaml:"Password"`
		Prefix             string `yaml:"Prefix"`
		DbPort             int    `yaml:"DbPort"`
		SetMaxIdleConns    int    `yaml:"SetMaxIdleConns"`
		SetMaxOpenConns    int    `yaml:"SetMaxOpenConns"`
		SetConnMaxLifetime int    `yaml:"SetConnMaxLifetime"`
	} `yaml:"MSSQLDB"`

	// 数据库类型
	DatabaseType struct {
		UseDbType string `yaml:"UseDbType"`
	} `yaml:"DatabaseType"`

	// 数据库日志文件
	DbLogFile struct {
		LogFile string `yaml:"LogFile"`
	} `yaml:"DbLogFile"`

	// 阿里云oss配置
	Uploader struct {
		Enable    string `yaml:"Enable"`
		AliyunOss struct {
			Host          string `yaml:"Host"`
			Bucket        string `yaml:"Bucket"`
			Endpoint      string `yaml:"Endpoint"`
			AccessId      string `yaml:"AccessId"`
			AccessSecret  string `yaml:"AccessSecret"`
			StyleSplitter string `yaml:"StyleSplitter"`
			StyleAvatar   string `yaml:"StyleAvatar"`
			StylePreview  string `yaml:"StylePreview"`
			StyleSmall    string `yaml:"StyleSmall"`
			StyleDetail   string `yaml:"StyleDetail"`
		} `yaml:"AliyunOss"`
		Local struct {
			Host string `yaml:"Host"`
			Path string `yaml:"Path"`
		} `yaml:"Local"`
	} `yaml:"Uploader"`

	// smtp
	Smtp struct {
		Host     string `yaml:"Host"`
		Port     string `yaml:"Port"`
		Username string `yaml:"Username"`
		Password string `yaml:"Password"`
		SSL      bool   `yaml:"SSL"`
	} `yaml:"Smtp"`

	// es
	Es struct {
		Url   string `yaml:"Url"`
		Index string `yaml:"Index"`
	} `yaml:"Es"`
}

func Init(filename string) *Config {
	Instance = &Config{}
	if yamlFile, err := ioutil.ReadFile(filename); err != nil {
		logrus.Error(err)
	} else if err = yaml.Unmarshal(yamlFile, Instance); err != nil {
		logrus.Error(err)
	}
	return Instance
}
