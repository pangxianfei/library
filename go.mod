module gitee.com/pangxianfei/library

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cast v1.5.0
	github.com/spf13/viper v1.15.0
	gopkg.in/yaml.v2 v2.4.0
)
